import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsService } from './products.service';
import { FacadeService } from './facade.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers:[
    ProductsService,

    FacadeService
  ]
})
export class ServicesModule { }
