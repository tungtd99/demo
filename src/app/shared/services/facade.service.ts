import { Injectable, Injector } from '@angular/core';
import { ProductsService } from './products.service';


@Injectable()
export class FacadeService {
  
  private _productService: ProductsService;
  public get productService(): ProductsService {
    if(!this._productService){
      this._productService = this.injector.get(ProductsService);
    }
    return this._productService;
  } 

  
  constructor(private injector: Injector) {

  }

  
  getProductDetails() {
    return this.productService.getProductDetails();
  }

  getProductOverview() {
    return this.productService.getProductOverview();
  }

}
