import { Injectable } from '@angular/core';

@Injectable()
export class ProductsService {

  constructor() { }
// Should call to HTTP services
  getProductDetails(){
    return 'product details from HTTP service'
  }

  getProductOverview(){
    return 'product overview from HTTP service'
  }
}
